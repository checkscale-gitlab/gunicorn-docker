# Build stage
FROM registry.gitlab.com/nevrona/public/poetry-docker:1.0.5 AS builder

ARG PYTHONUNBUFFERED=1
ARG POETRY_VIRTUALENVS_CREATE=0

RUN apk update \
    && apk add --no-cache \
        binutils==2.33.1-r0 \
        libc-dev==0.7.2-r0 \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/cache/apk/*

WORKDIR /src/
COPY pyproject.toml ./
COPY poetry.lock ./
RUN poetry export --format requirements.txt --output app.requirements.txt

# Install app
FROM python:3.8.2-alpine3.11

LABEL maintainer="Nevrona FOSS <foss@nevrona.org>"
LABEL org.opencontainers.image.title="Gunicorn Docker Image"
LABEL org.opencontainers.image.description="Docker image for Gunicorn"
LABEL org.opencontainers.image.vendor="Nevrona S. A."
LABEL org.opencontainers.image.authors="HacKan <hackan@nevrona.org>"
LABEL org.opencontainers.image.version="0.2.0"
LABEL org.opencontainers.image.ref.name="0.2.0"
LABEL org.opencontainers.image.licenses="MPL-2.0"
LABEL org.opencontainers.image.source="https://gitlab.com/nevrona/public/gunicorn-docker"
LABEL org.opencontainers.image.url="https://gitlab.com/nevrona/public/gunicorn-docker"
LABEL org.opencontainers.image.created=""
LABEL org.opencontainers.image.revision=""
LABEL org.nevrona.url="https://nevrona.org"

ARG PYTHONUNBUFFERED=1
ARG POETRY_VIRTUALENVS_CREATE=0

RUN apk update \
    && apk add --no-cache \
        binutils==2.33.1-r0 \
        libc-dev==0.7.2-r0 \
    && rm -rf /var/lib/apt/lists/* \
    && rm -rf /var/cache/apk/*

WORKDIR /tmp/app/
# Bring files from the build stage
COPY --from=builder /src/app.requirements.txt ./

# Install app
RUN pip install --no-cache-dir --requirement app.requirements.txt

WORKDIR /

RUN rm -rf /tmp/app

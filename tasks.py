"""Common tasks for Invoke."""
from invoke import task


@task
def lint_docker(ctx):
    """Lint Dockerfile."""
    ctx.run('sudo docker run --rm -i hadolint/hadolint < Dockerfile', echo=True,
            pty=True, echo_stdin=False)


@task(lint_docker)
def lint(ctx):
    """Lint code and static analysis."""


@task
def build_docker(ctx, tag='latest', no_cache=False):
    """Build Docker image."""
    no_cache_arg = '--no-cache ' if no_cache else ''
    ctx.run(f'sudo docker build --compress --pull --rm {no_cache_arg}'
            f'--tag registry.gitlab.com/nevrona/public/gunicorn-docker:{tag} .',
            echo=True, pty=True, echo_stdin=False)
